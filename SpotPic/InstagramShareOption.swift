//
//  InstagramShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class InstagramShareOption: NSObject, ShareOption {
    override var description: String {
        return "Instagram"
    }
    var buttonImage: UIImage {
       return UIImage(named: "InstagramButton")!
    }
    var isEnabled: Bool {
        return UIApplication.sharedApplication().canOpenURL(NSURL(string: "instagram://")!)
    }
    var interactionController = UIDocumentInteractionController()
    var successCallback: SharingSuccess = nil
    
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        successCallback = success
        
        let filePath = NSTemporaryDirectory().stringByAppendingPathComponent("SpotPic.igo")
        UIImageJPEGRepresentation(image, 1.0).writeToFile(filePath, atomically: false)
        
        if let fileURL = NSURL(fileURLWithPath: filePath, isDirectory: false) {
            interactionController.delegate = self
            interactionController.URL = fileURL
            interactionController.UTI = "com.instagram.exclusivegram"
            interactionController.annotation = ["InstagramCaption": "#SpotPic"]
            interactionController.presentOpenInMenuFromRect(CGRectZero, inView: container.view, animated: true)
        }
    }
  
}

extension InstagramShareOption: UIDocumentInteractionControllerDelegate {
    func documentInteractionController(controller: UIDocumentInteractionController, willBeginSendingToApplication application: String) {
        successCallback?()
    }
}