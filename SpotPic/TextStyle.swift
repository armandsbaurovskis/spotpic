//
//  TextStyle.swift
//  SpotPic
//
//  Created by Janis Jansons on 28/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class TextStyle: ScrollListItem {
    var font: UIFont?
    var label = UILabel()
    var shadows = [UILabel]()
    
    class var allStyles: [TextStyle] {
        return [
            TextStyle(fontName: "Anton", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Berkshire Swash", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Comfortaa", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Courgette", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Exo 2", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Grand Hotel", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Great Vibes", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Josefin Sans", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Kaushan Script", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Lobster", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Poiret One", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Roboto Slab", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Sacramento", labelSize: 30, previewSize: 20),
            TextStyle(fontName: "Ubuntu", labelSize: 30, previewSize: 20),
        ]
    }
    
    init(title: String, fontName: String, labelSize: CGFloat, previewSize: CGFloat) {
        super.init()
        
        let labelFont = UIFont(name: fontName, size: labelSize)
        let previewFont = UIFont(name: fontName, size: previewSize)
        
        self.title = title
        self.font = labelFont
        
        label.text = title
        label.font = previewFont
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.whiteColor()
        label.textAlignment = .Center
        preview.addSubview(label)
        
        // Adding thick solid text shadow
        let shadowHeight = 3
        for _ in 1...(2 * shadowHeight) {
            let shadow = UILabel()
            shadow.text = title
            shadow.font = previewFont
            shadow.adjustsFontSizeToFitWidth = true
            shadow.textColor = UIColor(white: 0.0, alpha: 0.1)
            shadow.textAlignment = .Center
            preview.insertSubview(shadow, belowSubview: label)
            shadows.append(shadow)
        }
    }
    
    convenience init(fontName: String, labelSize: CGFloat, previewSize: CGFloat) {
        self.init(title: fontName, fontName: fontName, labelSize: labelSize, previewSize: previewSize)
    }
    
    override func centerSubviews() {
        super.centerSubviews()
        
        var labelFrame = preview.bounds
        labelFrame.inset(dx: 5, dy: 0)
        label.frame = labelFrame

        // Stack text shadows at every half-point
        for (index, shadow) in enumerate(shadows) {
            var shadowFrame = labelFrame
            shadowFrame.origin.y += CGFloat(index) * 0.5
            shadow.frame = shadowFrame
        }
    }
}
