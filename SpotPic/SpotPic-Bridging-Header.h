//
//  SpotPic-Bridging-Header.h
//  SpotPic
//
//  Created by Armands Baurovskis on 10/12/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

#ifndef SpotPic_SpotPic_Bridging_Header_h
#define SpotPic_SpotPic_Bridging_Header_h

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIEcommerceProduct.h"
#import "GAIEcommerceProductAction.h"
#import "GAIEcommercePromotion.h"
#import "GAIFields.h"
#import "GAILogger.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"

#endif
