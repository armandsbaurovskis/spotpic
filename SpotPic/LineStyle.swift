//
//  LineStyle.swift
//  SpotPic
//
//  Created by Janis Jansons on 28/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class LineStyle: ScrollListItem {
    var maskImage: UIImage?
    var listImage: UIImage?
    var imageView: UIImageView!
    
    class var allStyles: [LineStyle] {
        return [
            LineStyle(style: "Straight"),
            LineStyle(style: "Post"),
            LineStyle(style: "Rough"),
            LineStyle(style: "SmallWave"),
            LineStyle(style: "Wave"),
            LineStyle(style: "ZigZag"),
        ]
    }

    init(style: String) {
        super.init()
        
        self.title = style
        maskImage = UIImage(named: "\(style)LineMask")
        listImage = UIImage(named: "\(style)LinePreview")
        
        imageView = UIImageView(image: listImage)
        imageView.contentMode = .Center
        preview.addSubview(imageView)
    }
    
    override func centerSubviews() {
        super.centerSubviews()
        
        imageView.frame = preview.bounds
    }
}
