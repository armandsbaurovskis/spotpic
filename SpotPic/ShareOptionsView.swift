//
//  ShareOptionsView.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class ShareOptionsView: UICollectionView {
    let items: [ShareOption] = [
        InstagramShareOption(),
        TwitterShareOption(),
        FacebookShareOption(),
        CopyShareOption(),
        AlbumShareOption(),
        MessageShareOption(),
        MailShareOption(),
        MoreShareOption(),
    ]
    
    var imageForSharing: UIImage?
    var sharingCount: Int = 0
    var containerViewController: UIViewController!
    var lastContentHeight: CGFloat = 0
    var onSuccess: (ShareOption -> ())?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        dataSource = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if contentSize.height != lastContentHeight {
            lastContentHeight = contentSize.height
            
            if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
                let cols: CGFloat = 4.0
                let rows: CGFloat = 2.0
                let size: CGFloat = lastContentHeight < 100.0 ? 38.0 : 50.0
                let padding: CGFloat = lastContentHeight < 100.0 ?  3.0 : 20.0
                let horizontalMargin: CGFloat = (frame.width - cols * size - (cols - 1) * padding) / 2.0
                let verticalMargin: CGFloat = (frame.height - rows * size - (rows - 1) * padding) / 2.0

                layout.itemSize = CGSize(width: size, height: size)
                layout.minimumInteritemSpacing = padding
                layout.minimumLineSpacing = padding
                layout.sectionInset = UIEdgeInsets(top: verticalMargin, left: horizontalMargin, bottom: verticalMargin, right: horizontalMargin)
            }
            
            reloadData()
        }
    }
    
    func sendGANotifications(item: ShareOption){
        
        // GA: User pressed a social button
        let tracker = GAI.sharedInstance().defaultTracker
        var vc = self.containerViewController as ViewController
        tracker.send(GAIDictionaryBuilder.createEventWithCategory("Social buttons", action:"Did press social button", label: item.description, value: nil).build())
        
        // GA: photo with place or not
        if self.containerViewController is ViewController {
            var vc = self.containerViewController as ViewController
            
            if vc.placeIsSet {
                 // GA: User font
                tracker.send(GAIDictionaryBuilder.createEventWithCategory("Design", action:"Fonts", label: vc.placeLabel.font.familyName, value: nil).build());
                // GA : Image with place
                tracker.send(GAIDictionaryBuilder.createEventWithCategory("Pictures", action:"Picture with place", label:nil, value: nil).build());
            }else{
                 // GA : Image without place
                 tracker.send(GAIDictionaryBuilder.createEventWithCategory("Pictures", action:"Picture without place", label:nil, value: nil).build());
            }
            
            // GA: User line
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("Design", action:"Lines", label: vc.selectedStyle?.title, value: nil).build());
            
        }
        
        
        // GA: Sharing multiple times
        if self.sharingCount > 0 {
            tracker.send(GAIDictionaryBuilder.createEventWithCategory("Sharing", action:"Picture was shared multiple times", label:nil, value: nil).build())
        }
        
        self.sharingCount++
    }
}

// MARK: - UICollectionViewDataSource
extension ShareOptionsView: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ShareCell", forIndexPath: indexPath) as UICollectionViewCell
        
        let item = items[indexPath.item]
        
        let button = buttonForCell(cell)
        button.frame = cell.bounds
        button.setImage(item.buttonImage, forState: .Normal)
        button.enabled = item.isEnabled
        
        return cell
    }
    
    func buttonForCell(cell: UICollectionViewCell) -> UIButton {
        let buttonTag = 100
        
        if let button = cell.contentView.viewWithTag(buttonTag) as? UIButton {
            return button
        }
        
        let button = UIButton(frame: cell.bounds)
        button.tag = buttonTag
        button.addTarget(self, action: Selector("cellButtonClicked:"), forControlEvents: .TouchUpInside)
        cell.contentView.addSubview(button)
        
        return button
    }
    
    func cellButtonClicked(sender: AnyObject?) {
        if let cell = sender?.superview??.superview as? UICollectionViewCell {
            if let indexPath = indexPathForCell(cell) {
                let item = items[indexPath.item]
                if let image = imageForSharing {
                    
                    item.shareImage(image, inViewController: containerViewController,
                        onSuccess: {
                            
                            self.sendGANotifications(item)
                            if let successCallback = self.onSuccess {
                                successCallback(item)
                            }
                            
                        },
                        onFailure: nil
                    )
                }
            }
        }
    }
}