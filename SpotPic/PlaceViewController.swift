//
//  PlaceViewController.swift
//  SpotPic
//
//  Created by Janis Jansons on 31/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import MapKit

class PlaceViewController: UIViewController {
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var enterPlaceButton: UIButton!
    @IBOutlet weak private var loadingIndicator: UIActivityIndicatorView!
    
    weak var placeAlertOkAction: UIAlertAction?
    
    private var venues = [Venue]()
    private let distanceFormatter = MKDistanceFormatter()
    var referenceCoordinate: CLLocationCoordinate2D?
    var selectedPlace: Place?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        distanceFormatter.unitStyle = .Abbreviated

        
        if let coordinate = referenceCoordinate {
            FourSquare.sharedInstance.venuesNearTo(coordinate) { (venues, error) in
                self.venues = venues
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadingIndicator.stopAnimating()
                    self.tableView.reloadData()
                })
                
                if error != nil {
                    println("DataError: '\(error)'")
                }
            }
        } else {
            self.loadingIndicator.stopAnimating()
        }
    }
    
    // UIAlertController
    @IBAction func enterPlaceTitle() {
        let alert = UIAlertController(title: "Where are you?", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.autocapitalizationType = .Sentences
            textField.keyboardAppearance = .Dark
            
            NSNotificationCenter.defaultCenter().addObserver(self,
                selector: Selector("textFieldDidChangeNotification:"),
                name: UITextFieldTextDidChangeNotification,
                object: textField)
        }

        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (alertAction: UIAlertAction!) in
            if let textField = alert.textFields?.first as? UITextField {
                if let coordinate = self.referenceCoordinate {
                    self.selectedPlace = Place(title: textField.text, coordinate: coordinate)
                }
            }
            
            self.performSegueWithIdentifier("FromPlaces", sender: self)
            
            return
        }))
        placeAlertOkAction = alert.actions.last as? UIAlertAction
        placeAlertOkAction?.enabled = false

        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func textFieldDidChangeNotification(notification: NSNotification) {
        let textField = notification.object as UITextField
        placeAlertOkAction?.enabled = countElements(textField.text) > 0
    }
}

// MARK: - UITableViewDataSource
extension PlaceViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return venues.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VenueCell", forIndexPath: indexPath) as TableViewCell
        let venue = venueAtIndexPath(indexPath)

        cell.textLabel?.text = venue.title
        cell.detailTextLabel?.text = distanceFormatter.stringFromDistance(venue.distance)
        
        return cell
    }
    
    func venueAtIndexPath(indexPath: NSIndexPath) -> Venue {
        return venues[indexPath.section]
    }
}

// MARK: - UITableViewDelegate
extension PlaceViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        let venue = venueAtIndexPath(indexPath)
        
        selectedPlace = Place(title: venue.title, coordinate: venue.coordinate)
        
        return indexPath
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
}