//
//  ShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
typealias SharingSuccess = (() -> ())?
typealias SharingFailure = (() -> ())?

protocol ShareOption: Printable {
    var buttonImage: UIImage { get }
    var isEnabled: Bool { get }
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure)
}

