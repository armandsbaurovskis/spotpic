//
//  ViewController.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import AssetsLibrary

class ViewController: UIViewController {
    // Header outlets
    @IBOutlet weak var showShareOptionsButton: UIButton!
    @IBOutlet weak var hideShareOptionsButton: UIButton!
    
    // Content outlets
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var outputView: UIView!
    @IBOutlet weak var outputMapView: MKMapView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var placeGradient: UIImageView!
    @IBOutlet weak var pictureButton: UIButton!
    @IBOutlet weak var changePictureButton: UIButton!
    @IBOutlet weak var choosePictureButton: UIButton!
    @IBOutlet weak var placeButton: UIButton!
    @IBOutlet weak var changePlaceButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var outputImageContainer: UIScrollView!
    @IBOutlet weak var outputImageView: UIImageView!
    @IBOutlet weak var outputImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var outputImageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var outputImageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var outputImageTrailingConstraint: NSLayoutConstraint!
    
    // Footer outlets
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var fontsListView: ScrollListView!
    @IBOutlet weak var linesListView: ScrollListView!
    @IBOutlet weak var shareOptionsView: ShareOptionsView!
    @IBOutlet weak var fontsListTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var linesListCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareOptionsTopConstraint: NSLayoutConstraint!
    
    // Video preview
    let captureSession = AVCaptureSession()
    let captureOutput = AVCaptureStillImageOutput()
    
    // Location
    let locationManager = CLLocationManager()
    var referenceCoordinate: CLLocationCoordinate2D?

    // Constants
    let animationDuration = 0.2
    //Style
    var selectedStyle:LineStyle?
    // State
    var pictureIsSet: Bool = false {
        didSet {
            if pictureIsSet {
                showButtons([
                    changePictureButton,
                    showShareOptionsButton,
                ], hideButtons: [
                    pictureButton,
                    choosePictureButton,
                    hideShareOptionsButton,
                ])
            } else {
                if inShareMode {
                    inShareMode = false
                }

                showButtons([
                    pictureButton,
                    choosePictureButton,
                ], hideButtons: [
                    changePictureButton,
                    showShareOptionsButton,
                    hideShareOptionsButton,
                ])
            }
        }
    }
    var placeIsSet: Bool = false {
        didSet {
            if placeIsSet {
                showButtons([
                    changePlaceButton
                ], hideButtons: [
                    placeButton
                ])
            } else {
                showButtons([
                    placeButton
                ], hideButtons: [
                    changePlaceButton
                ])
            }
            
            if !inShareMode {
                UIView.animateWithDuration(animationDuration) {
                    if self.placeIsSet {
                        self.fontsListTrailingConstraint.constant = self.footerView.bounds.midX
                        self.linesListCenterXConstraint.constant = self.linesListView.bounds.midX
                    } else {
                        self.fontsListTrailingConstraint.constant = 0
                        self.linesListCenterXConstraint.constant = 0
                    }
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    var inShareMode: Bool = false {
        didSet {
            if inShareMode {
                showButtons([
                    hideShareOptionsButton
                ], hideButtons: [
                    showShareOptionsButton
                ])
                
                view.layoutIfNeeded()
                UIView.animateWithDuration(animationDuration,
                    animations: {
                        self.fontsListTrailingConstraint.constant = 0
                        self.linesListCenterXConstraint.constant = self.footerView.bounds.width
                        self.view.layoutIfNeeded()
                    },
                    completion: { _ in
                        UIView.animateWithDuration(self.animationDuration) {
                            self.shareOptionsTopConstraint.constant = self.shareOptionsView.bounds.height
                            self.view.layoutIfNeeded()
                        }
                    }
                )
                
                
            } else {
                
                // Clear screen after sharing.
                if shareOptionsView.sharingCount > 0 {
                    if pictureIsSet {
                        changePicture()
                    }
                    
                    if placeIsSet {
                        changePlace()
                    }
                }
                
                if pictureIsSet {
                    showButtons([
                        showShareOptionsButton
                    ], hideButtons: [
                        hideShareOptionsButton
                    ])
                }
                
                view.layoutIfNeeded()
                UIView.animateWithDuration(animationDuration,
                    animations: {
                        self.shareOptionsTopConstraint.constant = 0
                        self.view.layoutIfNeeded()
                    },
                    completion: { _ in
                        UIView.animateWithDuration(self.animationDuration) {
                            if self.placeIsSet {
                                self.fontsListTrailingConstraint.constant = self.footerView.bounds.midX
                                self.linesListCenterXConstraint.constant = self.linesListView.bounds.midX
                            } else {
                                self.fontsListTrailingConstraint.constant = 0
                                self.linesListCenterXConstraint.constant = 0
                            }
                            self.view.layoutIfNeeded()
                        }
                    }
                )
            }
        }
    }
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Fonts list
        fontsListView.onSelect = { style in
            if let textStyle = style as? TextStyle {
                self.placeLabel.font = textStyle.font
            }
        }
        fontsListView.items = TextStyle.allStyles

        // Lines list
        linesListView.onSelect = { style in
            if let lineStyle = style as? LineStyle {
                self.selectedStyle = lineStyle
                if let maskImage = lineStyle.maskImage {
                    let maskView = UIImageView(image: maskImage)
                    maskView.contentMode = UIViewContentMode.TopLeft
                    
                    var bottomRect = self.outputMapView.bounds
                    bottomRect.origin.y += maskView.bounds.height
                    bottomRect.size.height -= maskView.bounds.height
                    
                    let bottomMaskLayer = CAShapeLayer()
                    bottomMaskLayer.path = UIBezierPath(rect: bottomRect).CGPath
        
                    let maskLayer = CALayer()
                    maskLayer.addSublayer(maskView.layer)
                    maskLayer.addSublayer(bottomMaskLayer)
        
                    self.outputMapView.layer.mask = maskLayer
                } else {
                    self.outputMapView.layer.mask = nil
                }
            }
        }
        linesListView.items = LineStyle.allStyles
        
        // LocationManager
        locationManager.delegate = self
        locationManager.distanceFilter = 50
        if locationManager.respondsToSelector("requestWhenInUseAuthorization") {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startUpdatingLocation()
        
        // Video
        startVideoPreview()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Animate main buttons
        pulsateButton(pictureButton)
        pulsateButton(placeButton)
    }
    

    // MARK: - UI actions
    @IBAction func takePicture() {
        let connection = captureOutput.connectionWithMediaType(AVMediaTypeVideo)
        
        if connection != nil {
            captureOutput.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: {
                (buffer, error) in
                
                if error == nil {
                    self.pictureIsSet = true
                    self.updateOutputImage(UIImage(data: AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)))

                    if self.updateReferenceCoordinateToUserLocation() {
                        if !self.placeIsSet {
                            self.zoomOnReferenceCoordinateAnimated(true)
                        }
                    }
                } else {
                    println(error)
                }
            })
        } else {
            showCameraServiceAlert()
        }
    }
    
    @IBAction func changePicture() {
        pictureIsSet = false
        updateOutputImage(nil)

        if updateReferenceCoordinateToUserLocation() {
            if !placeIsSet {
                zoomOnReferenceCoordinateAnimated(true)
            }
        }
    }
    
    @IBAction func changePlace() {
        placeIsSet = false
        placeLabel.text = nil
        placeGradient.hidden = true

        outputMapView.removeAnnotations(outputMapView.annotations)
        zoomOnReferenceCoordinateAnimated(true)
    }
    
    @IBAction func showShareOptions() {
        inShareMode = true
        shareOptionsView.containerViewController = self
        shareOptionsView.imageForSharing = imageForSharing()
        shareOptionsView.sharingCount = 0
        shareOptionsView.onSuccess = { option in
            var buttonImage: UIImage?
            switch option {
                case let shareOption as AlbumShareOption:
                    buttonImage = UIImage(named: "DoneSavingButton")
                case let shareOption as CopyShareOption:
                    buttonImage = UIImage(named: "DoneCopyingButton")
                default:
                    buttonImage = UIImage(named: "DoneSharingButton")
            }
            
            if let buttonImage = buttonImage {
                self.doneButton.setImage(buttonImage, forState: .Normal)
            }
            
            self.showDoneButton()
        }
    }
    
    @IBAction func hideShareOptions() {
        inShareMode = false
        
    }

    func showDoneButton() {
        doneButton.alpha = 0.0
        doneButton.hidden = false
        UIView.animateWithDuration(0.2,
            animations: {
                self.doneButton.alpha = 1.0
            },
            completion: { _ in
                dispatch_after_delay(2.0, dispatch_get_main_queue()) {
                    if !self.doneButton.hidden {
                        self.hideDoneButton()
                    }
                }
            }
        )
    }
    
    @IBAction func hideDoneButton() {
        UIView.animateWithDuration(0.5,
            animations: {
                self.doneButton.alpha = 0.0
            },
            completion: { _ in
                self.doneButton.hidden = true
            }
        )
    }
    

    // MARK: - Segues
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if identifier == "ToPlaces" {
            if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .Denied {
                return true
            } else {
                showLocationServiceAlert()
                return false
            }
        } else if identifier == "ToImagePicker" {
            if ALAssetsLibrary.authorizationStatus() == .Denied {
                showPhotosServiceAlert()
                return false
            }
        }
        
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.destinationViewController {
            case let controller as PlaceViewController:
                controller.referenceCoordinate = referenceCoordinate
            default:
                break
        }
    }
    

    @IBAction func unwindFromSegue(segue: UIStoryboardSegue) {
        switch segue.sourceViewController {
            case let controller as PlaceViewController:
                outputMapView.removeAnnotations(outputMapView.annotations)
                
                if let place = controller.selectedPlace {
                    placeIsSet = true
                    placeLabel.text = place.title
                    placeGradient.hidden = false
                    
                    outputMapView.addAnnotation(place)
                    zoomOnCoordinate(place.coordinate, animated: true)
                }
            
            case let controller as AlbumImagesViewController:
                if let asset = controller.selectedAsset {
                    pictureIsSet = true
                    updateOutputImage(UIImage(CGImage: asset.defaultRepresentation().fullScreenImage().takeUnretainedValue()))

                    var zoomOn = false
                    if let imageLocation = asset.valueForProperty(ALAssetPropertyLocation) as? CLLocation {
                        zoomOn = updateReferenceCoordinate(imageLocation.coordinate)
                    } else {
                        zoomOn = updateReferenceCoordinateToUserLocation()
                    }
                    
                    if !placeIsSet && zoomOn {
                        self.zoomOnReferenceCoordinateAnimated(true)
                    }
                }
            
            default:
                break
        }
    }
    
    
    // MARK: - Reference coordinate
    func updateReferenceCoordinate(coordinate: CLLocationCoordinate2D?) -> Bool {
        if coordinate != nil {
            referenceCoordinate = coordinate
            return true
        }
        
        return false
    }
    
    func updateReferenceCoordinateToUserLocation() -> Bool {
        return updateReferenceCoordinate(locationManager.location?.coordinate)
    }
    
    
    // MARK: - Service alerts
    func showServiceAlertWithTitle(title: String?) {
        let alert = UIAlertController(title: title, message: "Please allow access in Settings", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .Default) { action in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            return
        })
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func showLocationServiceAlert() {
        showServiceAlertWithTitle("SpotPic does not have access to your location")
    }
    
    func showCameraServiceAlert() {
        showServiceAlertWithTitle("SpotPic does not have access to camera")
    }
    
    func showPhotosServiceAlert() {
        showServiceAlertWithTitle("SpotPic does not have access to your photos")
    }
    
    
    // MARK: - Helper methods
    func imageForSharing() -> UIImage {
        let legalLabel = outputMapView.subviews.filter {
            $0 is UILabel
        }.first as? UILabel

        // Hide legal label before capturing
        legalLabel?.alpha = 0.0
        
        UIGraphicsBeginImageContextWithOptions(outputView.bounds.size, true, 0.0)
        outputView.drawViewHierarchyInRect(outputView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // Restore legal label when done
        legalLabel?.alpha = 0.5
        
        return image
    }
    
    func updateOutputImage(image: UIImage?) {
        if image != nil {
            previewView.hidden = true
            
            outputImageView.image = image
            updateScrollViewZoom()
        } else {
            previewView.hidden = false
            
            outputImageView.image = nil
        }
    }
    
    func startVideoPreview() {
        if captureSession.inputs.isEmpty {
            captureSession.sessionPreset = AVCaptureSessionPresetPhoto
            
            captureOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(captureOutput)
            
            let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            if captureDevice != nil {
                var error: NSError?
                let captureInput = AVCaptureDeviceInput(device: captureDevice, error: &error)
                
                if captureInput != nil {
                    captureSession.addInput(captureInput)
                }
            }

            view.layoutIfNeeded()
            
            let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            previewLayer.frame = previewView.bounds
            previewView.layer.addSublayer(previewLayer)
        }
        
        captureSession.startRunning()
    }
    
    func showButtons(show: [UIButton], hideButtons hide: [UIButton]) {
        for button in show {
            button.alpha = 0.0
            button.hidden = false
        }
        
        UIView.animateWithDuration(animationDuration,
            animations: {
                for button in show {
                    button.alpha = 1.0
                }
                
                for button in hide {
                    button.alpha = 0.0
                }
            },
            completion: { _ in
                for button in hide {
                    button.hidden = true
                }
            }
        )
    }
    
    func pulsateButton(button: UIButton) {
        UIView.animateKeyframesWithDuration(3.0, delay: 0.0,
            options: .Repeat | .AllowUserInteraction,
            animations: {
                UIView.addKeyframeWithRelativeStartTime(0.0, relativeDuration: 0.75) {}
                
                UIView.addKeyframeWithRelativeStartTime(0.75, relativeDuration: 0.05) {
                    button.transform = CGAffineTransformMakeScale(0.8, 0.8)
                }
                
                UIView.addKeyframeWithRelativeStartTime(0.8, relativeDuration: 0.2) {
                    button.transform = CGAffineTransformMakeScale(1.0, 1.0)
                }
            },
            completion: nil)
    }
}

// MARK: - UIScrollViewDelegate
extension ViewController: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return outputImageView
    }

    func updateScrollViewZoom() {
        if let image = outputImageView.image {
            let viewWidth = outputImageContainer.bounds.width
            let viewHeight = outputImageContainer.bounds.height
            
            let imageWidth = image.size.width
            let imageHeight = image.size.height
            
            
            // update zoom
            var minZoom = max(viewWidth / imageWidth, viewHeight / imageHeight)
            minZoom = min(minZoom, 1.0)
            outputImageContainer.minimumZoomScale = minZoom
            outputImageContainer.zoomScale = minZoom

            
            // center image
            let xOffset: CGFloat = (minZoom * imageWidth - viewWidth) / 2.0
            let yOffset: CGFloat = (minZoom * imageHeight - viewHeight) / 2.0
            
            // adjust scroll view offset
            outputImageContainer.contentOffset = CGPoint(x: xOffset, y: yOffset)
            
            // adjust constraints if image is too small
            outputImageLeadingConstraint.constant = max(-xOffset, 0.0)
            outputImageTopConstraint.constant = max(-yOffset, 0.0)
            view.layoutIfNeeded()
        }
    }
}

// MARK: - MKMapViewDelegate
extension ViewController: MKMapViewDelegate {
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        let reuseId = "pin"
        
        var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if pin == nil {
            pin = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pin.image = UIImage(named: "Pin")
            pin.centerOffset = CGPoint(x: 0.0, y: -18.5)
        } else {
            pin.annotation = annotation
        }
        
        return pin
    }

    func zoomOnCoordinate(coordinate: CLLocationCoordinate2D?, animated: Bool) {
        if coordinate != nil {
            let point = MKMapPointForCoordinate(coordinate!)
            let size = MKMapSize(width: 0.1, height: 0.1)
            var rect = MKMapRect(origin: point, size: size)

            var padding = UIEdgeInsetsZero
            padding.top = 45
            
            let diff = (40_000 - MKMapRectGetWidth(rect)) / 2.0
            rect = outputMapView.mapRectThatFits(MKMapRectInset(rect, -diff, 0), edgePadding: padding)
            
            outputMapView.setVisibleMapRect(rect, animated: animated)
        }
    }
    
    func zoomOnReferenceCoordinateAnimated(animated: Bool) {
        zoomOnCoordinate(referenceCoordinate, animated: animated)
    }
}


// MARK: - CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if referenceCoordinate == nil && !pictureIsSet && !placeIsSet {
            if updateReferenceCoordinateToUserLocation() {
                zoomOnReferenceCoordinateAnimated(false)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("didFailWithError: \(error)")
    }
}