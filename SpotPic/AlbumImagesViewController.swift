//
//  AlbumImagesViewController.swift
//  SpotPic
//
//  Created by Janis Jansons on 12/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import AssetsLibrary

class AlbumImagesViewController: UIViewController {
    @IBOutlet weak private var collectionView: UICollectionView!
    var dateFormatter = NSDateFormatter()
    var titles = [String]()
    var assets = [[ALAsset]]()
    
    var selectedGroup: ALAssetsGroup?
    var selectedAsset: ALAsset?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let group = selectedGroup {
            dateFormatter.dateFormat = "MMMM d"

            var allAssets = [ALAsset]()
            
            group.enumerateAssetsUsingBlock({ (asset, index, _) in
                if asset != nil {
                    allAssets.append(asset)
                } else {
                    allAssets.sort {
                        let lhsDate = $0.valueForProperty(ALAssetPropertyDate) as NSDate
                        let rhsDate = $1.valueForProperty(ALAssetPropertyDate) as NSDate
                        
                        return lhsDate.compare(rhsDate) == NSComparisonResult.OrderedAscending
                    }
                    
                    for asset in allAssets {
                        let date = asset.valueForProperty(ALAssetPropertyDate) as NSDate
                        let dateString = self.dateFormatter.stringFromDate(date)
                        
                        if self.titles.last != dateString {
                            self.titles.append(dateString)
                            self.assets.append([asset])
                        } else {
                            self.assets[self.assets.count - 1].append(asset)
                        }
                    }
                    
                    self.collectionView.reloadData()
                    
                    // Scroll to newest item
                    self.collectionView.layoutIfNeeded()
                    let yOffset = self.collectionView.contentSize.height - self.collectionView.bounds.height
                    if yOffset > 0 {
                        self.collectionView.contentOffset = CGPoint(x: 0.0, y: yOffset)
                    }
                }
            })
        }
    }
    
    @IBAction func backToAlbums() {
        navigationController?.popViewControllerAnimated(true)
    }
}

extension AlbumImagesViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return assets.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets[section].count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCell", forIndexPath: indexPath) as UICollectionViewCell

        let asset = assetAtIndexPath(indexPath)
        let sourceImage = asset.thumbnail().takeUnretainedValue()
        
        if let image = UIImage(CGImage: sourceImage) {
            let button = buttonForCell(cell)
            button.frame = cell.bounds
            button.setImage(image, forState: .Normal)
        }
        
        return cell
    }
    
    func buttonForCell(cell: UICollectionViewCell) -> UIButton {
        let buttonTag = 100
        
        if let button = cell.contentView.viewWithTag(buttonTag) as? UIButton {
            return button
        }

        let button = UIButton()
        button.tag = buttonTag
        button.addTarget(self, action: Selector("cellButtonClicked:"), forControlEvents: .TouchUpInside)
        button.imageView?.contentMode = .ScaleAspectFill
        cell.contentView.addSubview(button)
        
        return button
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "DateView", forIndexPath: indexPath) as AlbumDateView
        
        view.dateLabel.text = titleAtIndexPath(indexPath)
        
        return view
    }
    
    func assetAtIndexPath(indexPath: NSIndexPath) -> ALAsset {
        return assets[indexPath.section][indexPath.item]
    }
    
    func titleAtIndexPath(indexPath: NSIndexPath) -> String {
        return titles[indexPath.section]
    }
    
    func cellButtonClicked(sender: AnyObject?) {
        if let cell = sender?.superview??.superview as? UICollectionViewCell {
            if let indexPath = collectionView.indexPathForCell(cell) {
                selectedAsset = assetAtIndexPath(indexPath)
                performSegueWithIdentifier("FromImagePicker", sender: nil)
            }
        }
    }
}