//
//  Place.swift
//  SpotPic
//
//  Created by Janis Jansons on 31/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import MapKit

class Place: NSObject, MKAnnotation {
    var title: String
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
