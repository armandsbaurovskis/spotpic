//
//  AlbumViewController.swift
//  SpotPic
//
//  Created by Janis Jansons on 12/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import AssetsLibrary

class AlbumViewController: UIViewController {
    @IBOutlet weak private var tableView: UITableView!

    let library = ALAssetsLibrary()
    var groups = [ALAssetsGroup]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        library.enumerateGroupsWithTypes(ALAssetsGroupSavedPhotos | ALAssetsGroupAlbum | ALAssetsGroupPhotoStream,
            usingBlock: { (group, _) in
                if group != nil {
                    group.setAssetsFilter(ALAssetsFilter.allPhotos())
                    if group.numberOfAssets() > 0 {
                        self.groups.append(group)
                    }
                } else {
                    self.groups.sort { $0 < $1 }
                    self.tableView.reloadData()
                }
            },
            failureBlock: { (error) in
                println("Error \(error)")
            }
        )
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.destinationViewController {
        case let controller as AlbumImagesViewController:
            controller.selectedGroup = groups[tableView.indexPathForSelectedRow()!.section]
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource
extension AlbumViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return groups.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let group = groups[indexPath.section]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("AlbumCell", forIndexPath: indexPath) as TableViewCell
        cell.textLabel?.text = group.valueForProperty(ALAssetsGroupPropertyName) as? String ?? "Album"
        cell.detailTextLabel?.text = "\(group.numberOfAssets())"
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AlbumViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
}


// MARK: - ALAssetsGroup: Comparable
extension ALAssetsGroup: Comparable {}
public func <(lhs: ALAssetsGroup, rhs: ALAssetsGroup) -> Bool {
    let lhsType = lhs.valueForProperty(ALAssetsGroupPropertyType) as ALAssetsGroupType
    let rhsType = rhs.valueForProperty(ALAssetsGroupPropertyType) as ALAssetsGroupType
    
    if lhsType == rhsType {
        let lhsName = lhs.valueForProperty(ALAssetsGroupPropertyName) as String
        let rhsName = rhs.valueForProperty(ALAssetsGroupPropertyName) as String
        return lhsName < rhsName
    } else {
        switch (lhsType, rhsType) {
            // All Photos
            case (ALAssetsGroupType(ALAssetsGroupSavedPhotos), _):
                return true
            case (_, ALAssetsGroupType(ALAssetsGroupSavedPhotos)):
                return false
            
            // Created albums
            case (ALAssetsGroupType(ALAssetsGroupAlbum), _):
                return true
            case (_, ALAssetsGroupType(ALAssetsGroupAlbum)):
                return false
            
            // Photo streams
            case (ALAssetsGroupType(ALAssetsGroupPhotoStream), _):
                return true
            case (_, ALAssetsGroupType(ALAssetsGroupPhotoStream)):
                return false
            
            // Other
            default:
                return false
        }
    }
}
public func ==(lhs: ALAssetsGroup, rhs: ALAssetsGroup) -> Bool {
    return lhs.isEqual(rhs)
}