//
//  FourSquare.swift
//  SpotPic
//
//  Created by Janis Jansons on 31/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import MapKit

class FourSquare {
    private let clientID = "5HQ5EG2RVLMW13Z2CCQW3R51QU5GPO3FYSSUYT1TZH1FSLTO"
    private let clientSecret = "KYAVKND2KM5RB2C5UF2RHRPHDMPQGXUABGCG1QGIXBOKBVQT"
    private let apiVersion = 20140909

    class var sharedInstance: FourSquare {
        struct Static {
            static let instance = FourSquare()
        }
        return Static.instance
    }
    
    func venuesNearTo(coordinate: CLLocationCoordinate2D, callback: ([Venue], String?) -> ()) {
        let params = [
            "ll": "\(coordinate.latitude),\(coordinate.longitude)",
            "radius": String(500),
            "limit": String(50),
            "sortByDistance": String(1),
        ]
        
        NSURLSession.sharedSession().dataTaskWithURL(urlForEndpoint("venues/explore", params: params), completionHandler: {
            (data, _, error) in

            var venues = [Venue]()
            var errorString: String?
            
            if error == nil {
                let json = JSON(data: data)
                let responseCode = json["meta", "code"].int ?? 500

                if responseCode == 200 {
                    if let items = json["response", "groups", 0, "items"].array {
                        for item in items {
                            venues.append(Venue(json: item))
                        }
                    }
                } else {
                    errorString = json["meta", "errorMessage"].string
                }
            } else {
                errorString = error.localizedDescription
            }
            
            callback(venues, errorString)
            
        }).resume()
    }
    
    
    private func urlForEndpoint(endpoint: String, params: [String: String] = [:]) -> NSURL {
        var urlString = "https://api.foursquare.com/v2/\(endpoint)?client_id=\(clientID)&client_secret=\(clientSecret)&v=\(apiVersion)&m=foursquare"
        
        for (param, value) in params {
            urlString += "&\(param)=\(value)"
        }
        
        return NSURL(string: urlString)!
    }
}

struct Venue: Printable {
    let title: String
    let coordinate: CLLocationCoordinate2D
    var distance: CLLocationDistance = 0
    var description: String {
        return "Venue(name: '\(title)', coordinate: \(coordinate), distance: \(distance))"
    }
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
    
    init(json: JSON) {
        self.title = json["venue", "name"].string ?? "Venue"
        
        let latitude: CLLocationDegrees = json["venue", "location", "lat"].double ?? 0
        let longitude: CLLocationDegrees = json["venue", "location", "lng"].double ?? 0
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        self.distance = json["venue", "location", "distance"].double ?? 0
    }
}
