//
//  TableViewCell.swift
//  SpotPic
//
//  Created by Janis Jansons on 12/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBInspectable var detailWidth: CGFloat = 0.0
    @IBInspectable var detailHeight: CGFloat = 0.0

    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView()
        selectedBackgroundView.backgroundColor = backgroundColor?.colorWithAlphaComponent(0.5)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let horizontalPadding: CGFloat = 10.0
        let verticalPadding: CGFloat = (contentView.frame.height - detailHeight) / 2.0
        
        let detailFrame = CGRect(
            x: contentView.frame.width - horizontalPadding - detailWidth,
            y: verticalPadding,
            width: detailWidth,
            height: detailHeight
        )
        detailTextLabel?.frame = detailFrame

        if var textFrame = textLabel?.frame {
            textFrame.origin.x = horizontalPadding
            textFrame.size.width = detailFrame.minX - 2.0 * horizontalPadding
            textLabel?.frame = textFrame
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        detailTextLabel?.backgroundColor = UIColor.whiteColor()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        detailTextLabel?.backgroundColor = UIColor.whiteColor()
    }
}
