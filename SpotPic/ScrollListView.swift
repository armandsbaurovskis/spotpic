//
//  ScrollListView.swift
//  SpotPic
//
//  Created by Janis Jansons on 28/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class ScrollListView: UIScrollView {
    var frames = [CGRect]()
    let visibleItems = 3
    let totalItems = 5
    let selectedItemIndex = 2
    var items: [ScrollListItem] = [] {
        didSet {
            restoreUserDefault()
        }
    }
    var selectedItem: ScrollListItem? {
        didSet {
            if let callback = onSelect {
                if let item = selectedItem {
                    saveUserDefault()
                    callback(item)
                }
            }
        }
    }
    var itemHeight: CGFloat = 0.0 {
        didSet {
            if itemHeight != oldValue {
                contentSize = CGSize(
                    width: frame.width,
                    height: itemHeight * CGFloat(totalItems)
                )
                
                frames = []
                for index in 0..<totalItems {
                    let itemFrame = CGRect(
                        x: 0.0,
                        y: CGFloat(index) * itemHeight,
                        width: frame.width,
                        height: itemHeight
                    )

                    frames.append(itemFrame)
                }
            }
        }
    }
    var onSelect: (ScrollListItem -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        delegate = self
        bounces = false
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        let newHeight = frame.height / CGFloat(visibleItems)
        
        var forcedReload = false
        if itemHeight != newHeight {
            forcedReload = true
            
            itemHeight = newHeight
            contentOffset.y = itemHeight
        }

        reloadItemsIfNeeded(forced: forcedReload)
    }

    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.anyObject() as UITouch
        if touch.tapCount == 1 {
            let location = touch.locationInView(self)

            if CGRectContainsPoint(frames[selectedItemIndex - 1], location) {
                moveUp()
            } else if CGRectContainsPoint(frames[selectedItemIndex + 1], location) {
                moveDown()
            }
        }
    }
    
    func itemAtIndex(index: Int) -> ScrollListItem? {
        if let selectedItem = selectedItem {
            var targetIndex = find(items, selectedItem) ?? 0
            targetIndex += -selectedItemIndex + index
            
            if targetIndex < 0 {
                targetIndex += items.count
            }
            
            if targetIndex > items.count - 1 {
                targetIndex -= items.count
            }

            return items[targetIndex]
        }
        
        return nil
    }
    
    
    func reloadItemsIfNeeded(forced: Bool = false) {
        let midPoint = CGPoint(x: 0, y: frame.midY)

        var midFrame = frames[selectedItemIndex]
        midFrame.origin.y -= contentOffset.y

        if !CGRectContainsPoint(midFrame, midPoint) || forced {
            if !forced {
                if contentOffset.y <= itemHeight {
                    // If scrolling down
                    selectedItem = itemAtIndex(selectedItemIndex - 1)
                    contentOffset.y += itemHeight
                } else if contentOffset.y >= itemHeight {
                    // If scrolling up
                    selectedItem = itemAtIndex(selectedItemIndex + 1)
                    contentOffset.y -= itemHeight
                }
            } else {
                // Reselect item to force call didSet observer
                selectedItem = itemAtIndex(selectedItemIndex)
            }
            
            // Remove existing subviews
            self.subviews.map {
                $0.removeFromSuperview()
            }

            // (Re)add items
            for index in 0..<totalItems {
                if let item = itemAtIndex(index) {
                    self.addSubview(item.preview)
                    
                    item.preview.frame = frames[index]
                    item.centerSubviews()
                    
                    item.selected = index == selectedItemIndex
                }
            }
        }
    }
    
    func moveUp() {
        var updatedOffset = contentOffset
        updatedOffset.y = 0
        
        setContentOffset(updatedOffset, animated: true)
    }
    
    func moveDown() {
        var updatedOffset = contentOffset
        updatedOffset.y = itemHeight * 2
        
        setContentOffset(updatedOffset, animated: true)
    }
    
    
    // User defaults
    func restoreUserDefault() {
        if let itemTitle = NSUserDefaults.standardUserDefaults().objectForKey(userDefaultName()) as? String {
            let savedItem = items.filter{ $0.title == itemTitle }.first
            if savedItem != nil {
                selectedItem = savedItem
                return
            }
        }
        
        selectedItem = items.first
    }
    
    func saveUserDefault() {
        NSUserDefaults.standardUserDefaults().setObject(selectedItem?.title, forKey: userDefaultName())
    }
    
    func userDefaultName() -> String {
        return reflect(items.first).summary
    }
}

// MARK: - UIScrollViewDelegate
extension ScrollListView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        var updatedOffset = contentOffset
        updatedOffset.y = itemHeight
        
        setContentOffset(updatedOffset, animated: true)
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            var updatedOffset = contentOffset
            updatedOffset.y = itemHeight
            
            setContentOffset(updatedOffset, animated: true)
        }
    }
}
