//
//  AlbumShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class AlbumShareOption: NSObject, ShareOption  {
    override var description: String {
        return "Album"
    }
    var buttonImage: UIImage {
        return UIImage(named: "AlbumButton")!
    }
    var isEnabled: Bool {
        return true
    }
    
    var successCallback: SharingSuccess = nil
    var failureCallback: SharingFailure = nil
    
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        successCallback = success
        failureCallback = failure
        
        UIImageWriteToSavedPhotosAlbum(image, self, Selector("image:didFinishSavingWithError:contextInfo:"), nil)
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafePointer<()>) {
        if error != nil {
            println("AlbumShareFailed")
            failureCallback?()
        } else {
            successCallback?()
        }
    }
}