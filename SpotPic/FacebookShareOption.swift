//
//  FacebookShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import Social

class FacebookShareOption: NSObject, ShareOption  {
     override var description: String {
        return "Facebook"
    }
    var buttonImage: UIImage {
        return UIImage(named: "FacebookButton")!
    }
    var isEnabled: Bool {
        return SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)
    }
   
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        let shareController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        shareController.setInitialText("#SpotPic")
        shareController.addImage(image)
        shareController.completionHandler = { (result) in
            switch result {
                case .Done:
                    success?()
                default:
                    break
            }
        }
        
        container.presentViewController(shareController, animated: true, completion: nil)
    }
}