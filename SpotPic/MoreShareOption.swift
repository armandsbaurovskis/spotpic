//
//  MoreShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class MoreShareOption: NSObject, ShareOption  {
     override var description: String {
        return "More"
    }
    var buttonImage: UIImage {
        return UIImage(named: "MoreButton")!
    }
    var isEnabled: Bool {
        return true
    }
    
   
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        let shareController = UIActivityViewController(activityItems: [ image, "#SpotPic" ], applicationActivities: nil)
        shareController.excludedActivityTypes = [
            UIActivityTypePostToTwitter,
            UIActivityTypePostToFacebook,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypeMessage,
            UIActivityTypeMail,
            UIActivityTypeCopyToPasteboard,
        ]
        shareController.completionWithItemsHandler = { (_, completed, _, error) in
            if error != nil {
                println("MoreShareFailed")
                failure?()
            } else if completed {
                success?()
            }
        }
        
        container.presentViewController(shareController, animated: true, completion: nil)
    }
}