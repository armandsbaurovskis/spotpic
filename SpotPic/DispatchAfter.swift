//
//  DispatchAfter.swift
//  SpotPic
//
//  Created by Janis Jansons on 14/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import Foundation

func dispatch_after_delay(delay: NSTimeInterval, queue: dispatch_queue_t, block: dispatch_block_t) {
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
    dispatch_after(time, queue, block)
}