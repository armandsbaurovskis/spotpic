//
//  ScrollListItem.swift
//  SpotPic
//
//  Created by Janis Jansons on 28/10/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit


class ScrollListItem: Equatable, Printable {
    var title = ""
    var preview = UIView()
    var glow: UIImageView!
    var selected: Bool = true {
        didSet {
            if oldValue != selected {
                UIView.animateWithDuration(0.2) {
                    if self.selected {
                        self.preview.alpha = 1.0
                        self.glow.alpha = 1.0
                        
                        let transform = CGAffineTransformMakeScale(1.0, 1.0)
                        for subview in self.preview.subviews as [UIView] {
                            if subview != self.glow {
                                subview.transform = transform
                            }
                        }
                    } else {
                        self.preview.alpha = 0.3
                        self.glow.alpha = 0.0

                        let transform = CGAffineTransformMakeScale(0.7, 0.7)
                        for subview in self.preview.subviews as [UIView] {
                            if subview != self.glow {
                                subview.transform = transform
                            }
                        }
                    }
                }
            }
        }
    }
    
    init() {
        preview.userInteractionEnabled = false
        
        glow = UIImageView(image: UIImage(named: "Glow"))
        glow.contentMode = UIViewContentMode.ScaleAspectFit

        preview.addSubview(glow)
    }
    
    func centerSubviews() {
        glow.frame = preview.bounds
    }
}

extension ScrollListItem: Printable {
    var description: String {
        return title
    }
}

func ==(lhs: ScrollListItem, rhs: ScrollListItem) -> Bool {
    return lhs.title == rhs.title
}
