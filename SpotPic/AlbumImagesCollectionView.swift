//
//  AlbumImagesCollectionView.swift
//  SpotPic
//
//  Created by Janis Jansons on 13/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class AlbumImagesCollectionView: UICollectionView {
    var initialLayoutIsSet = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !initialLayoutIsSet {
            initialLayoutIsSet = true
            
            if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
                let cols: CGFloat = 4.0
                
                let size: CGFloat = floor((frame.width - (cols - 1) * layout.minimumInteritemSpacing) / cols)
                layout.itemSize = CGSize(width: size, height: size)
                
                reloadData()
            }
        }
    }
}