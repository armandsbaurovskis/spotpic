//
//  MessageShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import MessageUI

class MessageShareOption: NSObject, ShareOption  {
    override var description: String {
        return "Message"
    }
    var buttonImage: UIImage {
        return UIImage(named: "MessageButton")!
    }
    var isEnabled: Bool {
        return MFMessageComposeViewController.canSendAttachments()
    }
    
    var successCallback: SharingSuccess = nil
    var failureCallback: SharingFailure = nil
    
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        successCallback = success
        failureCallback = failure
        
        let shareController = MFMessageComposeViewController()
        shareController.messageComposeDelegate = self
        shareController.body = "#SpotPic"
        shareController.addAttachmentData(UIImagePNGRepresentation(image), typeIdentifier: "public.png", filename: "SpotPic.png")
        container.presentViewController(shareController, animated: true, completion: nil)
    }
}

extension MessageShareOption: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        switch result.value {
            case MessageComposeResultSent.value:
                successCallback?()
            case MessageComposeResultFailed.value:
                println("MessageShareFailed")
                failureCallback?()
            default:
                break
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}