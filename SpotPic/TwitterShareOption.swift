//
//  TwitterShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import Social

class TwitterShareOption: NSObject, ShareOption  {
     override var description: String {
        return "Twitter"
    }
    var buttonImage: UIImage {
        return UIImage(named: "TwitterButton")!
    }
    var isEnabled: Bool {
        return SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)
    }
  
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        let shareController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        
        shareController.setInitialText("#SpotPic")
        shareController.addImage(image)
        shareController.completionHandler = { result in
            switch result {
            case .Done:
                success?()
            default:
                break
            }
        }
        
        container.presentViewController(shareController, animated: true, completion: nil)
    }
}