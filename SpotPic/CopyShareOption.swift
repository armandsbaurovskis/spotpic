//
//  CopyShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class CopyShareOption: NSObject, ShareOption  {
     override var description: String {
        return "Copy"
    }
    var buttonImage: UIImage {
        return UIImage(named: "CopyButton")!
    }
    var isEnabled: Bool {
        return true
    }
   
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
    
        UIPasteboard.generalPasteboard().image = image
        success?()
    }
}