//
//  AlbumDateView.swift
//  SpotPic
//
//  Created by Janis Jansons on 13/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit

class AlbumDateView: UICollectionReusableView {
    @IBOutlet weak var dateLabel: UILabel!
    var initialLayoutIsSet = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !initialLayoutIsSet {
            dateLabel.frame = bounds
        }
    }
}
