//
//  MailShareOption.swift
//  SpotPic
//
//  Created by Janis Jansons on 09/11/14.
//  Copyright (c) 2014 IdeaBits. All rights reserved.
//

import UIKit
import MessageUI

class MailShareOption: NSObject, ShareOption  {
    override var description: String {
        return "Mail"
    }
    var buttonImage: UIImage {
        return UIImage(named: "MailButton")!
    }
    var isEnabled: Bool {
        return MFMailComposeViewController.canSendMail()
    }
    
    var successCallback: SharingSuccess = nil
    var failureCallback: SharingFailure = nil
    
    func shareImage(image: UIImage, inViewController container: UIViewController, onSuccess success: SharingSuccess, onFailure failure: SharingFailure) {
        successCallback = success
        failureCallback = failure
        
        let shareController = MFMailComposeViewController()
        shareController.mailComposeDelegate = self
        shareController.setMessageBody("#SpotPic", isHTML: false)
        shareController.addAttachmentData(UIImagePNGRepresentation(image), mimeType: "public.png", fileName: "SpotPic.png")        
        container.presentViewController(shareController, animated: true, completion: nil)
    }
}

extension MailShareOption: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        switch result.value {
            case MFMailComposeResultSent.value:
                successCallback?()
            case MFMailComposeResultSaved.value:
                successCallback?()
            case MFMailComposeResultFailed.value:
                println("MailShareFailed")
                failureCallback?()
            default:
                break
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}